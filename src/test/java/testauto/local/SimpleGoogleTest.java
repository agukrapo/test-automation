package testauto.local;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class SimpleGoogleTest {

    private WebDriver driver;

    @BeforeClass
    public void setUpTest() {
        log.info("Test setup");
        // Firefox driver from
        // https://github.com/mozilla/geckodriver/releases
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver-v0.22.0-macos");

        // Create a new instance of the Firefox driver
        // Notice that the remainder of the code relies on the interface,
        // not the implementation.
        driver = new FirefoxDriver();
    }

    @Test
    public void testTitle() {
        // And now use this to visit Google
        driver.navigate().to("http://www.google.com");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Cheese!");

        // Now submit the form. WebDriver will find the form for us from the element
        element.submit();

        // Log the title of the page
        log.info("Page title is: {}", driver.getTitle());

        // Google's search is rendered dynamically with JavaScript.
        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);

        // Wait until the title in lower case starts with cheese!
        wait.until(webDriver -> webDriver.getTitle().toLowerCase().startsWith("cheese!"));

        String title = driver.getTitle();

        // Should see: "cheese! - Google Search"
        log.info("Page title is: {}", title);

        // Title should start with: "cheese!"
        assertThat(title).startsWith("Cheese!");
    }

    @AfterClass
    public void tearDownTest() {
        log.info("Test teardown");
        // Close the browser
        driver.quit();
    }
}
